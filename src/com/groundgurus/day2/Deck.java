package com.groundgurus.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
  List<Card> cards = new ArrayList<>();

  public Deck() {
    init();
  }

  public void shuffle() {
    Collections.shuffle(cards);
  }

  public void printDeckOfCards() {
    for (int i = 0; i < cards.size(); i++) {
      Card card = cards.get(i);
      System.out.print(card.getDetails());

      if (i != cards.size() - 1) {
        System.out.print(", ");
      }
    }
    System.out.println();
  }

  private void init() {
    cards.add(new Card("Hearts", "Ace"));
    cards.add(new Card("Hearts", "Two"));
    cards.add(new Card("Hearts", "Three"));
  }
}
