package com.groundgurus.day2;

import java.util.List;

public class LambdaExpressionsExample {
  public static void main(String[] args) {
    // filter the array to only include even numbers then print it's values
    List<Integer> numbers = List.of(34, 68, 90, 589, 189, 509, 42);

    // filter the arraylist and print it's values
    numbers.stream()
      .filter(num -> num % 2 == 0)
      .map(num -> "Even: " + num) // List<Integer> to List<String>
      .forEach(System.out::println);
//      .forEach(num -> System.out.println(num));

//    // filter the arraylist
//    List<Integer> evenNumbers = new ArrayList<>();
//    for (Integer num : numbers) {
//      if (num % 2 == 0) {
//        evenNumbers.add(num);
//      }
//    }
//
//    // print the values of the arraylist
//    for (Integer num : evenNumbers) {
//      System.out.println(num);
//    }

//    numbers.stream()
//      .filter(num -> num % 2 == 0)
//      .forEach(System.out::println);
  }
}
