package com.groundgurus.day2;

public class CardMain {
  public static void main(String[] args) {
    Deck deckOfCards = new Deck();
    deckOfCards.shuffle();
    deckOfCards.printDeckOfCards();
  }
}
