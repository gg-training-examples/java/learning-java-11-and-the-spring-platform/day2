package com.groundgurus.day2;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetExample {
  public static void main(String[] args) {
    Set<String> mySet = new TreeSet<>();
    mySet.add("Hello");
    mySet.add("Hi");
    mySet.add("Hello");
    mySet.add("Howdy");
    mySet.add("Kamusta");
    mySet.add("Como Estas");

    System.out.println(mySet);
  }
}
