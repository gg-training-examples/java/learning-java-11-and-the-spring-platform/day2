package com.groundgurus.day2;

public class AnimalMain {
  public static void main(String[] args) {
    Dog myDog = new Dog();
    doTricks(myDog);

    Cat myCat = new Cat();
    doTricks(myCat);
  }
  // method overloading

  private static void doTricks(Animal animal) {
    if (animal instanceof Dog) {
      Dog dog = (Dog) animal;
      dog.fetchBall();
    } else if (animal instanceof Cat) {
      Cat cat = (Cat) animal;
      cat.doNothing();
    }
  }
}
