package com.groundgurus.day2.exercises;

public class Address {
  private String streetAddress1;
  private String streetAddress2;
  private String city;
  private String province;
  private String postalCode;

  public Address(String streetAddress1, String streetAddress2, String city, String province, String postalCode) {
    this.streetAddress1 = streetAddress1;
    this.streetAddress2 = streetAddress2;
    this.city = city;
    this.province = province;
    this.postalCode = postalCode;
  }

  public String getAddressDetails() {
    StringBuilder sb = new StringBuilder();

    sb.append("Street Address 1: ").append(streetAddress1).append("\n")
      .append("Street Address 2: ").append(streetAddress2).append("\n")
      .append("City: ").append(city).append("\n")
      .append("Province: ").append(province).append("\n")
      .append("Postal Code: ").append(postalCode).append("\n");

    return sb.toString();
  }

  @Override
  public String toString() {
    return getAddressDetails();
  }

  public String getStreetAddress1() {
    return streetAddress1;
  }

  public void setStreetAddress1(String streetAddress1) {
    this.streetAddress1 = streetAddress1;
  }

  public String getStreetAddress2() {
    return streetAddress2;
  }

  public void setStreetAddress2(String streetAddress2) {
    this.streetAddress2 = streetAddress2;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }
}
