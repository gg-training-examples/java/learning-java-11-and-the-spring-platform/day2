package com.groundgurus.day2.exercises;

import com.groundgurus.day2.Animal;

public class Mouse extends Animal {
  @Override
  public void speak() {
    System.out.println("Squick!");
  }
}
