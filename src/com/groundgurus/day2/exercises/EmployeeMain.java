package com.groundgurus.day2.exercises;

public class EmployeeMain {
  public static void main(String[] args) {
    Employee emp1 = new Employee("John", "M", "Doe", "Developer", "Development");
    Employee emp2 = new Employee("Joseph", "X", "Bourne", "Secret Agent", "Unknown", new Address("abc", "efg", "hij", "klm", "1234"));

    System.out.println(emp1.getDetails());
    System.out.println(emp2.getDetails());
  }
}
