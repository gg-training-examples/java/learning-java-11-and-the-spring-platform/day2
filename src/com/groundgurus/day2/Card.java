package com.groundgurus.day2;

public class Card {
  String suite;
  String name;

  public Card(String suite, String name) {
    this.suite = suite;
    this.name = name;
  }

  public String getDetails() {
    return name + " of "
      + suite;
  }
}

