package com.groundgurus.day2;

import java.util.ArrayList;
import java.util.List;

public class ListExample {
  public static void main(String[] args) {
    List<String> myList = new ArrayList<>();
    myList.add("Hello");
    myList.add("Hi");
    myList.add("Howdy");
    myList.add("Hello");

    System.out.println(myList);
    System.out.println(myList.get(1));
  }
}
