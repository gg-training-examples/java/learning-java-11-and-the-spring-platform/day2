package com.groundgurus.day2;

public class Dog extends Animal {
  public void speak() {
    System.out.println("Bark!");
  }

  public void fetchBall() {
    System.out.println("Woof woof! Run.");
  }
}
