package com.groundgurus.day2;

public class Cat extends Animal {
  public void speak() {
    System.out.println("Meow!");
  }

  public void doNothing() {
    System.out.println("Looks away..");
    System.out.println("Sleep...");
  }
}
